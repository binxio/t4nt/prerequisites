# T4NT: Prerequisites

Tech for non-techies is a workshop aimed at making technology and software engineering in particular, more approachable for those from more traditional backgrounds. The workshop requires you to do a little work beforehand (but not too much so don't panic)! Just follow the instructions below then come along an join!

## 0: DO NOT SHOW UP WITHOUT A LAPTOP! THIS IS A HANDS ON WORKSHOP!

## 1: Install Google Chrome
Download and install [Google Chrome](https://www.google.com/chrome/) so we are all using the same browser! :)

## 2: Install Docker for Windows/Mac
What Docker is is not important! :D We will cover this in the workshop. For now, just download and install [Docker for Windows/Mac](https://docs.docker.com/install/). Please be sure to test that it installed correctly. Docker will not work on Windows home so ensure that you are using either Windows Pro or Windows Enterprise.

Test the installation on Mac by opening up "terminal" or on Windows by opening up the "command prompt" and typing `docker run -t hello-world`. When you hit enter Docker should say hello, if there is an error thrown then something has gone wrong with the installation! Please follow the instructions on the website carefully, particularly for Windows as you may need to enable certain features in the operating system. 

## 3: Install git
Git is a version control system (VCS) used by developers across the world to manage their code repositories. You can install git from the [git-scm](https://git-scm.com/downloads) website. To test if the installation is successful on Mac by opening up "terminal" or on Windows by opening up the "command prompt" and typing `git --version`. If no error is thrown and you are shownt eh git version then you have installed git correctly! :)
